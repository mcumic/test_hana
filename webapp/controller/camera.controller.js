sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ndc/BarcodeScanner",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel",
	"sap/m/Dialog"
], function (Controller, BarcodeScanner, MessageToast, JSONModel, Dialog) {
	"use strict";

	return Controller.extend("a1.camera.camera.controller.camera", {
		onInit: function () {
			this.objs = {
				barcode: ""
			};
			var oModel = new JSONModel(this.objs);
			this.getOwnerComponent().setModel(oModel);
		},
		onBCPress: function (oEvent) {
			var me = this;
			BarcodeScanner.scan(
				function (mResult) {
					if (!mResult.cancelled) {
						me.objs.barcode = mResult.text;
						var oModel = new JSONModel(me.objs);
						me.getOwnerComponent().setModel(oModel);
					}
				},
				function (Error) {
					MessageToast.show("Scanning failed: " + Error);
				}
			);
		},
		onCamPress: function (oEvent) {
			var me = this;
			if (this.oDialog) {
				this.oDialog.open();
				this.cameraStart();
				return;
			}
			this.oDialog = new Dialog({
				title: "Click on capture to take photo",
				beginButton: new sap.m.Button({
					text: "Capture",
					press: function (oBtnEvent) {
						me.setImage();
						me.oDialog.close();
					},
					type: "Accept"
				}),
				content: [
					new sap.ui.core.HTML({
						content: "<video id=\"player\" autoplay></video>"
					}),
					new sap.m.Input({
						placeholder: "Enter image name",
						required: true
					})
				],
				endButton: new sap.m.Button({
					text: "Close",
					press: function (oBtnEvent) {
						me.oDialog.close();
						me.cameraStop();
					},
					type: "Reject"
				})
			});

			this.getView().addDependent(this.oDialog);
			this.oDialog.open();
			this.cameraStart();
		},
		setImage: function () {
			var me = this;
			// var base64Img;
			this.imageCapture($("#player")[0].srcObject).then(function (a) {
				var canvas = $("#pictureCanvas")[0];
				var context = canvas.getContext("2d");
				context.drawImage(a, 0, 0, a.width, a.height);
				// base64Img = canvas.toDataURL();
				me.cameraStop();
			});
		},
		cameraStart: function () {
			navigator.mediaDevices.getUserMedia({
					video: {
						facingMode: "environment"
					}
				}).then(function (stream) {
					$("#player")[0].srcObject = stream;
				});
		},
		cameraStop: function () {
			$("#player")[0].srcObject.getTracks().forEach(function (track) {
				track.stop();
			});
		},
		imageCapture: function (mediaStream) {
			var mediaStreamTrack = mediaStream.getVideoTracks()[0];
			var imageCapture = new ImageCapture(mediaStreamTrack);
			return imageCapture.grabFrame();
		}
	});
});
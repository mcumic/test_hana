/*global QUnit*/

sap.ui.define([
	"a1/camera/camera/controller/camera.controller"
], function (Controller) {
	"use strict";

	QUnit.module("camera Controller");

	QUnit.test("I should test the camera controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});
/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"a1/camera/camera/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});